\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Theory}{2}
\contentsline {section}{\numberline {3}Experimental Setup}{3}
\contentsline {section}{\numberline {4}Method}{4}
\contentsline {subsection}{\numberline {4.1}Determination of Planck's Constant}{4}
\contentsline {subsection}{\numberline {4.2}Current-voltage characteristic at a specific light frequency}{4}
\contentsline {section}{\numberline {5}Results}{5}
\contentsline {section}{\numberline {6}Discussion}{7}
\contentsline {section}{\numberline {7}Conclusion}{7}
\contentsline {section}{\numberline {8}Appendix 1}{8}
\contentsline {section}{\numberline {9}Appendix 2}{10}
\contentsline {section}{References}{12}
